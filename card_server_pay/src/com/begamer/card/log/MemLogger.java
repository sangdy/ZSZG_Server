package com.begamer.card.log;

import org.apache.log4j.Logger;

/**
 * 内存日志
 * @author LiTao
 * 2014-1-27 上午10:11:59
 */
public class MemLogger
{
	public static final Logger logger=Logger.getLogger("MemLogger");
	
	public static void print(String s)
	{
		logger.info(s);
	}
}
