package com.begamer.card.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.controller.GmStatisticPlayerController;
import com.begamer.card.model.pojo.PkRank;
import com.begamer.card.model.pojo.Player;

public class GmStatisticPlayerDao extends HibernateDaoSupport {

	private static Logger logger = Logger.getLogger(GmStatisticPlayerController.class);

	/** 查询玩家排行榜* */
	@SuppressWarnings("unchecked")
	public List<PkRank> getPkRanks()
	{
		logger.info("finding PkRanks instance");
		try
		{
			return getHibernateTemplate().find("from PkRank r");
		}
		catch (RuntimeException e)
		{
			logger.debug("find failed" + e);
			throw e;
		}
	}

	/** 玩家等级排序* */
	@SuppressWarnings("unchecked")
	public List<Player> levelUp()
	{
		logger.info("order by levelUp instance");
		try
		{
			return getHibernateTemplate()
					.find("from Player p order by p.level");
		}
		catch (RuntimeException e)
		{
			logger.info("order by levelUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Player> levelDown()
	{
		logger.info("order by levelDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.level desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by levelUp failed" + e);
			throw e;
		}
	}

	/** 战斗力排序* */
	@SuppressWarnings("unchecked")
	public List<Player> battlePowerUp()
	{
		logger.info("order by battlePowerUp instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.battlePower");
		}
		catch (RuntimeException e)
		{
			logger.info("order by battlePowerUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Player> battlePowerDown()
	{
		logger.info("order by battlePowerDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.battlePower desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by battlePowerDown failed" + e);
			throw e;
		}
	}

	/** 排行榜排序* */
	@SuppressWarnings("unchecked")
	public List<PkRank> pkRankUp()
	{
		logger.info("order by pkRankUp instance");
		try
		{
			return getHibernateTemplate().find("from PkRank p order by p.rank");
		}
		catch (RuntimeException e)
		{
			logger.info("order by pkRankUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PkRank> pkRankDown()
	{
		logger.info("order by pkRankDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from PkRank p order by p.rank desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by pkRankDown failed" + e);
			throw e;
		}
	}

	/** 剩余钻石排序* */
	@SuppressWarnings("unchecked")
	public List<Player> crystalUp()
	{
		logger.info("order by crystalUp instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.crystal");
		}
		catch (RuntimeException e)
		{
			logger.info("order by crystalUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Player> crystalDown()
	{
		logger.info("order by crystalDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.crystal desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by crystalDown failed" + e);
			throw e;
		}
	}

	/** vipLevel排序* */
	@SuppressWarnings("unchecked")
	public List<Player> vipLevelUp()
	{
		logger.info("order by vipLevelUp instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.vipLevel");
		}
		catch (RuntimeException e)
		{
			logger.info("order by vipLevelUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Player> vipLevelDown()
	{
		logger.info("order by vipLevelDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.vipLevel desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by vipLevelDown failed" + e);
			throw e;
		}
	}

	/** 本月签到次数排序* */
	@SuppressWarnings("unchecked")
	public List<Player> signTimesUp()
	{
		logger.info("order by signTimesUp instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.signTimes");
		}
		catch (RuntimeException e)
		{
			logger.info("order by signTimesUp failed" + e);
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Player> signTimesDown()
	{
		logger.info("order by signTimesDown instance");
		try
		{
			return getHibernateTemplate().find(
					"from Player p order by p.signTimes desc");
		}
		catch (RuntimeException e)
		{
			logger.info("order by signTimesDown failed" + e);
			throw e;
		}
	}
}
