package com.begamer.card.common;

/**
 * 
 * @ClassName: Constant
 * @Description: TODO 程序内部静态属性
 * @author gs
 * @date Nov 10, 2011 3:40:52 PM
 * 
 */
public class Constant {

	public static final Integer LOAD_ALL = new Integer(0);
	// 验证码图片session
	public static final String IMAGE_SESSION = "IMAGE_RAND";
	// 登录用户session
	public static final String LOGIN_USER = "user";

	// 分页默认参数
	public static final Integer PAGEID_DEFAULT = new Integer(1);
	public static final Integer PAGESIZE_MIDDLE = new Integer(20);

	// GM拥有权限
	public static final String GM_PRIVILEGE = "qx";
	// 所有功能模块
	public static final String GM_MODEL = "mk";
	// 密钥
	public static final String Key = "key";
	// 暴击倍率
	public static final float CriMulRate = 2f;
	// 最大回合数
	public static final int MaxRound = 30;
	// 卡牌个数
	public static final int CardNum = 6;
	/** 最大金币数10亿 **/
	public static final int MaxGold = 10 * 10000 * 10000;
	/** 最大水晶数10亿 **/
	public static final int MaxCrystal = 10 * 10000 * 10000;
	/** 最大屌丝券数10亿 **/
	public static final int MaxDiaos = 10 * 10000 * 10000;
	/** 最大友情值 **/
	public static final int MaxFriendValue = 10 * 10000 * 10000;
	/** 最大符文值 **/
	public static final int MaxRuneNum = 10 * 10000 * 10000;
	/** 最大登录天数 **/
	public static final int MaxLoginDayNum = 10 * 10000 * 10000;
	/** 最大抽卡幸运值 **/
	public static final int MaxLuckNum = 10 * 10000 * 10000;
	/** 符文页数 **/
	public static final int RunePageLength = 6;
	/** 好友援护友情值增量 **/
	public static final int FriendHelpValue = 25;
	/** 随机玩家援护友情值增量 **/
	public static final int RandomPlayerHelpValue = 10;
	/** 刷新援护列表需要的水晶数 **/
	public static final int RefreshHelpNeedCrystalNum = 20;
	/** 卡组默认合体技 **/
	public static final String DefaultFormationUnitSkillIds = "50001-50002";
	/** 玩家默认援护技Id **/
	public static final int DefaultHelpUnitSkillId = 50001;
	/** 登录抽卡幸运值 **/
	public static final int LoginLucky = 25;
	/** 运营额外抽卡幸运值 **/
	public static int extendLuckNum = 0;
	/** 掉落总概率 **/
	public static final int TotalDropPro = 10000;
	/** 刷新未申请为好友的玩家最大个数 **/
	public static final int MaxFefreshUnapplyNum = 5;
	/** 最大申请好友数 **/
	public static final int MaxApplyFriendNum = 10;
	/** 好友申请的有效时间 **/
	public static final int MaxApplyFriendValidTime = 7 * 24 * 60 * 60 * 1000;
	/** 最大体力值 **/
	public static final int MaxPower = 120;
	/** 自动回复体力间隔 **/
	public static final int AutoRestorePowerTime = 5 * 60 * 1000;
	/** 屌丝卷Id **/
	public static final int DiaosItemId = 40000;
	/** 第二突破次数 **/
	public static final int SecondBreakNum = 2;
	/** 最大突破次数 **/
	public static final int MaxBreakNum = 5;
	/** 初始怒气上限 **/
	public static final int InitMaxEnergy = 200;
	/** pve精英起始Id **/
	public static final int PveMissionType2 = 1;
	/** 每天免费邀请好友战斗次数 **/
	public static final int FreeInviteFriendTimes = 5;
	/** 领取礼包通信密码 **/
	public static final String GiftPassword = "card3bwlanD";
	/** 未读邮件保留时间7天(单位:分钟) **/
	public static final int MailKeepTime1 = 7 * 24 * 60;
	/** 已读邮件保留时间1小时(单位:分钟) **/
	public static final int MailKeepTime2 = 60;
	/** 未读邮件保存时间1天（单位：分钟） **/
	public static final int MailKeepTime3 = 1 * 24 * 60;
	/** 客户端操作系统 **/
	public static final String OS_ANDROID = "android";
	public static final String OS_IOS = "ios";
	public static final String OS_PC = "pc";
	/** 初始玩家数量 **/
	public static final int InitPlayerNum = 50;
	/** 黑市刷新时间--6个小时 **/
	public static final int blackRefreshTime = 60 * 60 * 6;

}
