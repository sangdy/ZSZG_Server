package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class FriendCostData implements PropertyReader
{
	/**购买次数**/
	public int number;
	/**提升上限**/
	public int number1;
	/**花费类型:1钻石,2金币**/
	public int type;
	/**花费**/
	public int cost;

	private static HashMap<Integer, FriendCostData> data=new HashMap<Integer, FriendCostData>();
	
	@Override
	public void addData()
	{
		data.put(number, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	public static FriendCostData getData(int number)
	{
		return data.get(number);
	}

}
