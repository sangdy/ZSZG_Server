package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.List;

import com.begamer.card.common.util.StringUtil;

public class EquippropertyData implements PropertyReader
{
	public int type;
	public int level;
	public int[] starNumbers;
	
	private static List<EquippropertyData> data=new ArrayList<EquippropertyData>();
	
	@Override
	public void addData()
	{
		data.add(this);
	}
	
	@Override
	public void resetData()
	{
		data.clear();
	}
	
	@Override
	public void parse(String[] ss)
	{
		type=StringUtil.getInt(ss[0]);
		level=StringUtil.getInt(ss[1]);
		int length=ss.length-2;
		starNumbers=new int[length];
		for(int i=0;i<length;i++)
		{
			starNumbers[i]=StringUtil.getInt(ss[2+i]);
		}
		addData();
	}
	
	public static EquippropertyData getData(int type,int level)
	{
		for(EquippropertyData ed:data)
		{
			if(ed.type==type && ed.level==level)
			{
				return ed;
			}
		}
		return null;
	}

}
