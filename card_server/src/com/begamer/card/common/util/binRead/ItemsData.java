package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ItemsData implements PropertyReader {
	
	public int id;
	
	public int number;
	
	public int type;
	
	public String name;
	
	public int star;
	
	public int fragment;
	public int goodztype;
	public int goodsid;
	
	public int use;
	
	public int usetimes;
	
	public String discription;
	
	public int pile;
	
	public int sell;
	
	public int icon;
	
	public int sound;
	
	private static HashMap<Integer, ItemsData> data = new HashMap<Integer, ItemsData>();
	private static List<ItemsData> dataList=new ArrayList<ItemsData>();
	
	@Override
	public void addData()
	{

		data.put(id, this);
		dataList.add(this);
	}
	
	@Override
	public void parse(String[] ss)
	{

	}
	
	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	public static ItemsData getItemsData(int itemsId)
	{

		return data.get(itemsId);
	}
	
	/** *******获得所有item********** */
	public static List<ItemsData> getAllItemsData()
	{
		return dataList;
	}
	
	public static ItemsData getItemsDataByTypeStar(int t,int s)
	{
		for(int i=0;i<dataList.size();i++)
		{
			if(dataList.get(i).type==t && dataList.get(i).star ==s)
			{
				return dataList.get(i);
			}
		}
		return null;
	}
}
