package com.begamer.card.log;

import org.apache.log4j.Logger;

public class BattleCheckLogger
{
	public static final Logger logger=Logger.getLogger("BattleCheckLogger");
	
	public static void print(String s)
	{
		logger.info(s);
	}
}
