package com.begamer.card.json.command;

import java.util.List;

import com.begamer.card.json.BasicJson;

public class IntensifyJson extends BasicJson
{
	/**1角色卡,2主动技能卡,3,被动技能卡,4装备**/
	public int type;
	/**强化者索引**/
	public int index;
	/**被吞噬者索引**/
	public List<Integer> list;
	
	public int numType;//多次强化标识（0,单次强化，1多次强化）//强化次数
	
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public int getIndex()
	{
		return index;
	}
	public void setIndex(int index)
	{
		this.index = index;
	}
	public List<Integer> getList()
	{
		return list;
	}
	public void setList(List<Integer> list)
	{
		this.list = list;
	}
	public int getNumType() {
		return numType;
	}
	public void setNumType(int numType) {
		this.numType = numType;
	}
	
}
