package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class GiftRewardResultJson extends ErrorJson
{
	public int nextGift;//下一个礼包的id，如果为0则为不存在下一个礼包
	public int nextOnline;//距离下一个礼包领取的时间
	public int linetime;//如果当前礼包不可领取，返回剩余时间
	public int getNextGift() {
		return nextGift;
	}
	public void setNextGift(int nextGift) {
		this.nextGift = nextGift;
	}
	public int getNextOnline() {
		return nextOnline;
	}
	public void setNextOnline(int nextOnline) {
		this.nextOnline = nextOnline;
	}
	public int getLinetime() {
		return linetime;
	}
	public void setLinetime(int linetime) {
		this.linetime = linetime;
	}
	
	
}
