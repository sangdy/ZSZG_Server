package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class MazeRewardResultJson extends ErrorJson
{
	public String reward;//type-item-pro(type为1-5时，item格式为物品id,数量   其他为数量)

	public String getReward() {
		return reward;
	}

	public void setReward(String reward) {
		this.reward = reward;
	}
}
