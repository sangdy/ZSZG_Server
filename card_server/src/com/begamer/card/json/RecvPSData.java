package com.begamer.card.json;

public class RecvPSData
{
	private int status;
	private String username;
	private long userid;
	
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public long getUserid()
	{
		return userid;
	}
	public void setUserid(long userid)
	{
		this.userid = userid;
	}
}