<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'closeCachePage.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

	<script type="text/javascript">
		function check()
		{
			if(confirm("确定关服？"))
			{
				document.form1.submit();
				document.getElementById("ok").disabled="disabled";
			}
		}
	</script>
	
  </head>
  
  <body>
  	<form action="gm.htm?action=closeCache" name="form1" method="post">
	    <table>
	    	<tr>
	    		<td colspan="2">
	    			<font color="red">请先保存玩家数据,然后联系开发人员关闭服务器</font>
	    		</td>
	    	</tr>
	    	<tr>
	    		<td><font color="red">关服密钥*</font></td>
	    		<td><input type="text" value="" name="key" style="width: 300px"/></td>
	    	</tr>
	    	<tr>
	    		<td>关服前保存玩家数据</td>
	    		<td>
	    			<input id="ok" type="button" value="点击保存" onclick="javascript:check()">
	    		</td>
	    	</tr>
	    </table>
    </form>
  </body>
</html>
